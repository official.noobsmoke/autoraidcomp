import { constants } from "../constants";
import { generateCompositions } from "../ui/generateCompositions";

let affectedSheets: string[] = [];

export const changeDetection = {
    onEdit(e: any) {
        const activeSheet = SpreadsheetApp.getActiveSheet();
        const sheetName = activeSheet.getName();
        if (sheetName === constants.sheets.compositionTemplates.name && !affectedSheets.some(x => x === sheetName)) {
            affectedSheets.push(sheetName);
        }
        if (sheetName === constants.sheets.generateCompositions.name && e.range.getA1Notation() === constants.sheets.generateCompositions.selectedCompositionTemplatePosition)
        {
            generateCompositions.countCompositionClassAndSpecStatistics();
        }
    },
    refreshData() {
    }
}