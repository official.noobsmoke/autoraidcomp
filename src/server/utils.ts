declare interface String {
    format(...strings: any[]): string;
}

if (!String.prototype.format) {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            if (typeof args[number] === 'object') {
                args[number] = JSON.stringify(args[number]);
            }
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };
}