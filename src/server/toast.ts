export const toast = {
  show(message: string, type: MessageType) {
    SpreadsheetApp.getActive().toast(message, type);
  },
};