import { characterService } from "../service/characterService";
import { settingsLoader } from "../service/settingsLoader";
import { compositionTemplatesService } from "../service/compositionTemplatesService";
import { changeDetection } from "./changeDetection";
import { generateCompositions } from "../ui/generateCompositions";
import { compositionGenerator } from "../compositionGenerators/compositionGenerator";
import { generatedCompositions } from "../ui/generatedCompositions";

declare global {
  function onOpen(): any;
  function generateCompositionButton(): any;
  function onEdit(e: any): any;
  function showAdminSidebar(): any;
  function displayToast(): any;
  function countCompositionClassAndSpecStatistics(): any;
}

global.onOpen = () => {
  compositionTemplatesService.loadSpreadsheetData();
  generateCompositions.load();
  generateCompositions.countCompositionClassAndSpecStatistics();
  // SpreadsheetApp
  //   .getUi()
  //   .createMenu("Admin")
  //   .addItem("Admin page", "showAdminSidebar")
  //   .addToUi();
};

global.showAdminSidebar = () => {
  var widget = HtmlService.createHtmlOutputFromFile("compositionPreview.html");
  widget.setTitle("Admin page");
  SpreadsheetApp.getUi().showSidebar(widget);
};

global.generateCompositionButton = () => {
  loadSpreadSheetData();
  const compositions = compositionGenerator.generate();
  generatedCompositions.displayCompositions(compositions);
};

global.onEdit = (e) => {
  loadSpreadSheetData();
  changeDetection.onEdit(e);
};

global.countCompositionClassAndSpecStatistics = () => {
  loadSpreadSheetData();
  return generateCompositions.countCompositionClassAndSpecStatistics();
};

const loadSpreadSheetData = () => {
  settingsLoader.loadSettings();
  compositionTemplatesService.loadSpreadsheetData();
  characterService.loadSpreadsheetData();
};
