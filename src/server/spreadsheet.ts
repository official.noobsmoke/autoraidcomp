import { constants } from "../constants";

export const spreadsheet = {
  getSheetValues(name: string) {
    const sheet = this.getSheetByName(name);
    const data = sheet.getDataRange().getValues();
    for (let i = 0; i < data.length; i++) {
      for (let j = 0; j < data[i].length; j++) {
        let rowValue = data[i][j];
        if (typeof rowValue === "string" || rowValue instanceof String) {
          rowValue = rowValue.toLowerCase();
        }
      }
    }
    return data;
  },
  getSheetByName(name: string) {
    const sheet = SpreadsheetApp.getActive().getSheetByName(name);
    if (!sheet) {
      throw new Error(constants.error.sheetNotFound.format(name));
    }
    return sheet;
  },
  getSheetCellValue(name: string, value: string) {
    const sheet = this.getSheetByName(name);
    const rowValue = sheet.getRange(value).getValue();
    if (typeof rowValue === "string" || rowValue instanceof String) {
      return rowValue.toLowerCase();
    }
    return rowValue;
  },
};
