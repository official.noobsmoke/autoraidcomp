type PreviewSetupResult = {
  possibleCompsEstimation: PossibleCompsEstimation;
  charactersStatisticInfo: CharactersStatisticInfo[];
};

type PossibleCompsEstimation = {
  value: number;
  className: Class;
  spec: Specialization;
};
