type SpecAndCharacters = {
    spec: Specialization,
    characters: Character[]
}