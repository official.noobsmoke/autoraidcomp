type TemplateRaidSlot = {
  number: number;
  possibleClassAndSpecs: ClassAndSpec[];
  realm?: RSRealm;
};
