type CompositionTemplate = {
  name: string;
  instance: Instance;
  playersCount: number;
  raidSlots: TemplateRaidSlot[];
};
