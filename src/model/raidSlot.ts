type RaidSlot = {
  slotNumber: number;
  character: Character;
  spec: Specialization;
  isEmpty: boolean;
  realm?: RSRealm;
};
