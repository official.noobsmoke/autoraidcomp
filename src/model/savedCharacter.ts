type SavedCharacter = {
    characterName: string,
    instances: Instance[]
}