const enum Class {
    warrior = "warrior",
    paladin = "paladin",
    hunter = "hunter",
    rogue = "rogue",
    priest = "priest",
    deathknight = "deathknight",
    shaman = "shaman",
    mage = "mage",
    warlock = "warlock",
    druid = "druid",
}