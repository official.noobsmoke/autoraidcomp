type CharactersStatisticInfo = {
  classAndSpec: ClassAndSpec;
  availableCount: number;
  requiredCount: number;
  ratio: number;
};
