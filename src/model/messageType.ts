const enum MessageType {
    Information = "🧾Information",
    Error = "⚠️Error"
}