type Character = {
  name: string;
  mainName: string;
  className: Class;
  specDetails: SpecDetail[];
}

type SpecDetail = {
  spec: Specialization;
  fitness: number;
}