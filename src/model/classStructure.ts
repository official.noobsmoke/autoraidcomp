type ClassStructure = {
    class: Class,
    specsAndChars: SpecAndCharacters[],
}