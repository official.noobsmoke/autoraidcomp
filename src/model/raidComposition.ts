type RaidComposition = {
  number: number;
  instance: Instance;
  players: number;
  benchedPlayers: string[];
  slots: RaidSlot[];
};
