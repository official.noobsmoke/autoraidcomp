type ClassAndSpec = {
    class: Class,
    spec: Specialization
}