import { generateCompositions } from "../ui/generateCompositions";
import { compositionTemplatesService } from "../service/compositionTemplatesService";
import { characterService } from "../service/characterService";
import { characterRepository } from "../repository/characterRepository";
import { compositionGeneratorService } from "../service/compositionGeneratorService";
import _ from "lodash";

export const compositionGenerator = {
  generate() {
    const compositionTemplate = compositionTemplatesService.getCurrentCompositionTemplateByName();
    let availableCharacters: ClassStructure[] = [];
    let availableCharactersSnapshot: ClassStructure[] = [];
    const maxNumberOfCompositions = generateCompositions.getMaxNumberOfCompositionsPosition();
    let generatedCompositionCount = 0;
    let raidCompositions: RaidComposition[] = [];

    while (generatedCompositionCount < maxNumberOfCompositions) {
      let raidComposition: RaidComposition = {
        instance: compositionTemplate.instance,
        number: generatedCompositionCount + 1,
        players: compositionTemplate.playersCount,
        slots: [],
        benchedPlayers: [],
      };
      availableCharacters = getAvailableCharacters(raidComposition.instance, raidCompositions);
      availableCharactersSnapshot = JSON.parse(JSON.stringify(availableCharacters)); //Todo: find a better way to clone
      generateComposition(availableCharacters, compositionTemplate, raidComposition);
      if (!raidComposition) {
        continue;
      }
      raidComposition.benchedPlayers = compositionGeneratorService.getBenchedPlayers(availableCharacters);
      if (raidComposition.benchedPlayers && compositionGeneratorService.anyEmptySlot(raidComposition.slots)) {
        fillRemainingSlots(raidComposition, availableCharactersSnapshot);
      }
      raidCompositions.push(raidComposition);
      generatedCompositionCount++;
    }
    return raidCompositions;
  },
};

const generateComposition = (
  availableCharacters: ClassStructure[],
  compositionTemplate: CompositionTemplate,
  currentRaidComposition: RaidComposition
) => {
  for (let i = 0; i < compositionTemplate.playersCount; i++) {
    let mostNeededClassAndSpec = compositionGeneratorService.getAvailableAndRequiredStatistics(compositionTemplate, availableCharacters);
    mostNeededClassAndSpec = _.orderBy(mostNeededClassAndSpec, function (o) {
      return o.ratio;
    });
    for (let j = 0; j < mostNeededClassAndSpec.length; j++) {
      const slotToFill = compositionGeneratorService.getSlotByClassAndSpec(
        mostNeededClassAndSpec[j].classAndSpec,
        compositionTemplate.raidSlots,
        currentRaidComposition
      );
      if (slotToFill.length > 0 && !compositionGeneratorService.isSlotFilled(slotToFill[0].number, currentRaidComposition)) {
        fillSlot(slotToFill[0], compositionTemplate, availableCharacters, currentRaidComposition);
        break;
      }
    }
  }
  return currentRaidComposition;
};

const fillSlot = (
  slot: TemplateRaidSlot,
  compositionTemplate: CompositionTemplate,
  availableCharacters: ClassStructure[],
  currentRaidComposition: RaidComposition
) => {
  const possibleClassAndSpecs = slot.possibleClassAndSpecs;
  let slotRole: ClassAndSpec = slot.possibleClassAndSpecs[0];
  if (possibleClassAndSpecs.length > 1) {
    slotRole = multipleClassAndSpecs(slot, currentRaidComposition.slots, compositionTemplate, availableCharacters);
  }
  let character: Character;
  try {
    character = compositionGeneratorService.getTopFitness(slotRole.class, slotRole.spec, availableCharacters);
  } catch (err) {
    compositionGeneratorService.markSlotAsEmpty(slot.number, slotRole, currentRaidComposition);
    return null;
  }
  compositionGeneratorService.addCharacterToRaidCompositionAs(character, slotRole.spec, currentRaidComposition, slot.number, false, slot.realm);
  compositionGeneratorService.removeCharacterCharactersFromAvailable(character, availableCharacters);
  return true;
};

const fillRemainingSlots = (raidComposition: RaidComposition, availableCharactersSnapshot: ClassStructure[]) => {
  const emptySlots = raidComposition.slots.filter((x) => x.isEmpty);
  emptySlots.map((x) => {
    const charactersThatCanFill = compositionGeneratorService.getAllCharactersForClassAndSpecFromAvailable(
      x.character.className,
      x.spec,
      availableCharactersSnapshot
    );
    const benchedPlayers = raidComposition.benchedPlayers;
    let benchedPlayerCharacters: ClassStructure[] = [];
    for (let i = 0; i < benchedPlayers.length; i++) {
      const playerCharacters = compositionGeneratorService.getAllCharactersForMainFromAvailable(benchedPlayers[i], availableCharactersSnapshot);
      benchedPlayerCharacters = benchedPlayerCharacters.concat(playerCharacters);
    }
    for (let i = 0; i < charactersThatCanFill.length; i++) {
      const characterThatCanFill = charactersThatCanFill[i];

      let slot = raidComposition.slots.find((x) => x.character.mainName == characterThatCanFill.mainName);
      if (!slot) {
        continue;
      }
      let substituteCharacter: Character;
      try {
        substituteCharacter = compositionGeneratorService.getTopFitness(slot.character.className, slot.spec, benchedPlayerCharacters);
      } catch (err) {
        continue;
      }

      slot.character = substituteCharacter;
      x.character = characterThatCanFill;
      x.isEmpty = false;
      raidComposition.benchedPlayers = raidComposition.benchedPlayers.filter((x) => x !== substituteCharacter.mainName);
      break;
    }
  });
};

const getAvailableCharacters = (instance: Instance, raidCompositions: RaidComposition[]) => {
  const allCharacters = characterRepository.getCharacters();
  let characters = [...characterService.getAvailableCharactersForInstance(instance, allCharacters)];
  raidCompositions.map((x) => {
    x.slots.map((y) => {
      compositionGeneratorService.removeCharacterFromAvailable(y.character, characters);
    });
  });
  return characters;
};

const multipleClassAndSpecs = (
  templateRaidSlot: TemplateRaidSlot,
  raidSlots: RaidSlot[],
  compositionTemplate: CompositionTemplate,
  availableCharacters: ClassStructure[]
): ClassAndSpec => {
  let priorityClassAndSpecs = compositionGeneratorService.getNeededBuffersFromSlot(raidSlots, templateRaidSlot.possibleClassAndSpecs);
  if (compositionTemplate.instance == Instance.rs) {
    priorityClassAndSpecs = compositionGeneratorService.getNeededBuffersFromSlot(
      raidSlots.filter((x) => x.realm == templateRaidSlot.realm),
      templateRaidSlot.possibleClassAndSpecs
    );
  }
  if (priorityClassAndSpecs.length === 0) {
    const nonBuffers = templateRaidSlot.possibleClassAndSpecs.filter((x) => !compositionGeneratorService.isClassAndSpecBuffer(x));
    if (nonBuffers) {
      priorityClassAndSpecs = nonBuffers;
    } else {
      priorityClassAndSpecs = priorityClassAndSpecs.concat(templateRaidSlot.possibleClassAndSpecs);
    }
  }
  if (priorityClassAndSpecs.length === 1) {
    return priorityClassAndSpecs[0];
  }
  const characterStatistics = compositionGeneratorService.getAvailableAndRequiredStatistics(compositionTemplate, availableCharacters);
  const calculatedMaxNumberOfCompositions = compositionGeneratorService.calculatePossibleCompositionsEstimation(characterStatistics);
  const mostNeededClassAndSpec: ClassAndSpec = {
    class: calculatedMaxNumberOfCompositions.className,
    spec: calculatedMaxNumberOfCompositions.spec,
  };
  let leastNeededClass: CharactersStatisticInfo = {
    classAndSpec: mostNeededClassAndSpec,
    ratio: 0,
    availableCount: 0,
    requiredCount: 0,
  };
  characterStatistics.map((x) => {
    if (priorityClassAndSpecs.some((y) => _.isEqual(y, x.classAndSpec))) {
      if (!_.isEqual(x, mostNeededClassAndSpec)) {
        if (x.ratio > leastNeededClass.ratio) {
          leastNeededClass.classAndSpec = x.classAndSpec;
          leastNeededClass.ratio = x.ratio;
        }
      }
    }
  });
  return leastNeededClass.classAndSpec;
};
