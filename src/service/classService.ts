export const classService = {
  getMeleeHasteClasses() {
    return [
      {
        class: Class.deathknight,
        spec: Specialization.bloodtank,
      },
      {
        class: Class.deathknight,
        spec: Specialization.frostdps,
      },
      {
        class: Class.shaman,
        spec: Specialization.enhancement,
      },
    ] as ClassAndSpec[];
  },
  getRangeHasteClasses() {
    return [
      {
        class: Class.shaman,
        spec: Specialization.restoration,
      },
      {
        class: Class.shaman,
        spec: Specialization.elemental,
      },
      {
        class: Class.shaman,
        spec: Specialization.enhancement,
      },
    ] as ClassAndSpec[];
  },
  getMangleClasses() {
    return [
      {
        class: Class.druid,
        spec: Specialization.feraldps,
      },
      {
        class: Class.druid,
        spec: Specialization.feraltank,
      },
    ] as ClassAndSpec[];
  },
  getEmptyClassStructure() {
    return [
      {
        class: Class.warrior,
        specsAndChars: [
          {
            spec: Specialization.arms,
            characters: [] as Character[],
          },
          {
            spec: Specialization.fury,
            characters: [] as Character[],
          },
          {
            spec: Specialization.protection,
            characters: [] as Character[],
          },
        ],
      },
      {
        class: Class.paladin,
        specsAndChars: [
          {
            spec: Specialization.holy,
            characters: [] as Character[],
          },
          {
            spec: Specialization.protection,
            characters: [] as Character[],
          },
          {
            spec: Specialization.retribution,
            characters: [] as Character[],
          },
        ],
      },
      {
        class: Class.hunter,
        specsAndChars: [
          {
            spec: Specialization.beastmastery,
            characters: [] as Character[],
          },
          {
            spec: Specialization.marksmanship,
            characters: [] as Character[],
          },
          {
            spec: Specialization.survival,
            characters: [] as Character[],
          },
        ],
      },
      {
        class: Class.rogue,
        specsAndChars: [
          {
            spec: Specialization.assassination,
            characters: [] as Character[],
          },
          {
            spec: Specialization.combat,
            characters: [] as Character[],
          },
          {
            spec: Specialization.subtlety,
            characters: [] as Character[],
          },
        ],
      },
      {
        class: Class.priest,
        specsAndChars: [
          {
            spec: Specialization.discipline,
            characters: [] as Character[],
          },
          {
            spec: Specialization.holy,
            characters: [] as Character[],
          },
          {
            spec: Specialization.shadow,
            characters: [] as Character[],
          },
        ],
      },
      {
        class: Class.deathknight,
        specsAndChars: [
          {
            spec: Specialization.blooddps,
            characters: [] as Character[],
          },
          {
            spec: Specialization.bloodtank,
            characters: [] as Character[],
          },
          {
            spec: Specialization.frostdps,
            characters: [] as Character[],
          },
          {
            spec: Specialization.frosttank,
            characters: [] as Character[],
          },
          {
            spec: Specialization.unholydps,
            characters: [] as Character[],
          },
          {
            spec: Specialization.unholytank,
            characters: [] as Character[],
          },
        ],
      },
      {
        class: Class.shaman,
        specsAndChars: [
          {
            spec: Specialization.elemental,
            characters: [] as Character[],
          },
          {
            spec: Specialization.enhancement,
            characters: [] as Character[],
          },
          {
            spec: Specialization.restoration,
            characters: [] as Character[],
          },
        ],
      },
      {
        class: Class.mage,
        specsAndChars: [
          {
            spec: Specialization.arcane,
            characters: [] as Character[],
          },
          {
            spec: Specialization.fire,
            characters: [] as Character[],
          },
          {
            spec: Specialization.frost,
            characters: [] as Character[],
          },
        ],
      },
      {
        class: Class.warlock,
        specsAndChars: [
          {
            spec: Specialization.affliction,
            characters: [] as Character[],
          },
          {
            spec: Specialization.demonology,
            characters: [] as Character[],
          },
          {
            spec: Specialization.destruction,
            characters: [] as Character[],
          },
        ],
      },
      {
        class: Class.druid,
        specsAndChars: [
          {
            spec: Specialization.balance,
            characters: [] as Character[],
          },
          {
            spec: Specialization.feraldps,
            characters: [] as Character[],
          },
          {
            spec: Specialization.feraltank,
            characters: [] as Character[],
          },
          {
            spec: Specialization.restoration,
            characters: [] as Character[],
          },
        ],
      },
    ] as ClassStructure[];
  },
};
