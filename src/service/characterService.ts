import { constants } from "../constants";
import { sheetMapper } from "../mapper/sheetMapper";
import { characterRepository } from "../repository/characterRepository";
import { spreadsheet } from "../server/spreadsheet";

let savedCharacters: SavedCharacter[] = [];
let availablePlayers: string[] = [];

export const characterService = {
  loadSpreadsheetData() {
    savedCharacters = [];
    availablePlayers = [];
    characterRepository.deleteAllCharacters();
    loadSavedCharacters();
    loadAvailablePlayers();
    loadImportedCharacters();
  },
  getAvailableCharactersForInstance(instance: Instance, charactersPool: ClassStructure[]) {
    return charactersPool.map((x) => {
      return {
        ...x,
        specsAndChars: x?.specsAndChars?.map((x) => {
          return {
            ...x,
            characters: x.characters.filter((y) => availablePlayers.includes(y.mainName) && isCharacterFreeForInstance(y.name, instance)),
          };
        }),
      };
    }) as ClassStructure[];
  },
  getAvailableCharactersForSpecCount(className: Class, spec: Specialization, instance: Instance, charactersPool: ClassStructure[]) {
    const availableCharacters = this.getAvailableCharactersForInstance(instance, charactersPool);
    const characters = availableCharacters.find((x) => x.class === className)?.specsAndChars.find((y) => y.spec === spec)?.characters;
    if (!characters) {
      return 0;
    }
    return characters.length;
  },
};

const isCharacterFreeForInstance = (characterName: string, instance: Instance) => {
  return !savedCharacters.some((x) => x.characterName === characterName && x.instances.includes(instance));
};

const loadSavedCharacters = () => {
  const data = spreadsheet.getSheetValues(constants.sheets.savedCharacters.name);
  for (let i = 0; i < data.length; i++) {
    const savedCharacter = sheetMapper.sheetRowToSavedCharacter(data[i]);
    savedCharacters.push(savedCharacter);
  }
  return savedCharacters;
};

const loadAvailablePlayers = () => {
  const data = spreadsheet.getSheetValues(constants.sheets.availablePlayers.name);
  for (let i = 0; i < data.length; i++) {
    availablePlayers.push(data[i][0]);
  }
  return availablePlayers;
};

const loadImportedCharacters = () => {
  const data = spreadsheet.getSheetValues(constants.sheets.importedCharacters.name);
  for (let i = 0; i < data.length; i++) {
    const character = sheetMapper.sheetRowToCharacter(data[i]);
    characterRepository.addCharacter(character);
  }
};
