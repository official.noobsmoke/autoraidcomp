import { constants } from '../constants';
import { spreadsheet } from '../server/spreadsheet';
import { sheetMapper } from '../mapper/sheetMapper';
import '../server/utils';

let settings: Setting[] = [];

export const settingsLoader = {
  loadSettings() {
    const data = spreadsheet.getSheetValues(constants.sheets.settings.name)
    for (let i = 0; i < data.length; i++) {
      const setting = sheetMapper.sheetRowToSetting(data[i]);
      settings.push(setting);
    }
  },
  getSettings() {
    return settings;
  },
  getSettingValue(name: string) {
    const setting = settings.find(x => x.name === name);
    if (!setting) {
      throw new Error(constants.error.settingNotFound.format(name));
    }
    return setting.value;
  },
  clearSettings() {
    settings = [];
  }
};