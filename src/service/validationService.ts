import { constants } from "../constants";
import { classService } from "./classService";

let emptyClassStructure = classService.getEmptyClassStructure();

export const validationService = {
  validateCharacter(character: Character) {
    if (!validateClassName(character.className)) {
      throw new Error(constants.error.classNameNotValid.format(character));
    }
    if (!validateSpecs(character.className, character.specDetails)) {
      throw new Error(constants.error.specNotValid.format(character));
    }
    return true;
  },
};

const validateClassName = (className: string) => {
  return emptyClassStructure.some((x) => x.class == className);
};

const validateSpecs = (className: string, specDetails: SpecDetail[]) => {
  const classSetup = emptyClassStructure.find((x) => x.class == className);
  if (!classSetup) {
    return false;
  }
  for (let i = 0; i < specDetails.length; i++)
    if (!classSetup.specsAndChars.some((x) => x.spec === specDetails[i].spec)) {
      return false;
    }
  return true;
};
