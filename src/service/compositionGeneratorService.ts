import { characterService } from "./characterService";
import _ from "lodash";
import { classService } from "./classService";
import { compositionTemplatesService } from "./compositionTemplatesService";
import { constants } from "../constants";

export const compositionGeneratorService = {
  getAvailableAndRequiredStatistics(compositionTemplate: CompositionTemplate, charactersPool: ClassStructure[]) {
    const classStructure = classService.getEmptyClassStructure();
    let characterStatisticInfo: CharactersStatisticInfo[] = [];
    classStructure.map((x) =>
      x.specsAndChars.map((y) => {
        const requiredCharacters = compositionTemplatesService.getRequiredCharactersForSpec(x.class, y.spec, compositionTemplate);
        const availableCharactersCount = characterService.getAvailableCharactersForSpecCount(
          x.class,
          y.spec,
          compositionTemplate.instance,
          charactersPool
        );
        if (requiredCharacters == 0) {
          return;
        }
        characterStatisticInfo.push({
          classAndSpec: {
            class: x.class,
            spec: y.spec,
          },
          availableCount: availableCharactersCount,
          requiredCount: requiredCharacters,
          ratio: availableCharactersCount / requiredCharacters,
        });
      })
    );
    return characterStatisticInfo;
  },
  calculatePossibleCompositionsEstimation(charactersStatistcInfo: CharactersStatisticInfo[]) {
    let maxCompositions = Number.MAX_VALUE;
    let className: Class = Class.deathknight;
    let spec: Specialization = Specialization.frost;
    charactersStatistcInfo.map((x) => {
      const ratio = x.availableCount / x.requiredCount;
      if (ratio < maxCompositions) {
        maxCompositions = ratio;
        className = x.classAndSpec.class;
        spec = x.classAndSpec.spec;
      }
    });
    return {
      className,
      spec,
      value: maxCompositions,
    } as PossibleCompsEstimation;
  },
  removeCharacterCharactersFromAvailable(character: Character, availableCharacters: ClassStructure[]) {
    availableCharacters = availableCharacters.map((a) => {
      return {
        ...a,
        specsAndChars: (a.specsAndChars = a.specsAndChars?.map((x) => {
          return {
            ...x,
            characters: x.characters.filter((y) => y.name !== character.name && y.mainName !== character.mainName),
          } as SpecAndCharacters;
        })),
      };
    });
  },
  removeCharacterFromAvailable(character: Character, availableCharacters: ClassStructure[]) {
    availableCharacters = availableCharacters.map((a) => {
      return {
        ...a,
        specsAndChars: (a.specsAndChars = a.specsAndChars?.map((x) => {
          return {
            ...x,
            characters: x.characters.filter((y) => y.name !== character.name),
          } as SpecAndCharacters;
        })),
      };
    });
  },
  getTopFitness(className: Class, spec: Specialization, availableCharacters: ClassStructure[]) {
    const characters = availableCharacters.find((x) => x.class == className)?.specsAndChars.find((x) => x.spec == spec)?.characters;
    if (!characters) {
      throw new Error(constants.error.characterNotFound);
    }
    let maxFitnessValue = 0;
    let topFitnessCharacter: Character | undefined;
    characters?.map((x) => {
      x.specDetails.map((y) => {
        if (y.spec == spec && y.fitness > maxFitnessValue) {
          topFitnessCharacter = x;
          maxFitnessValue = y.fitness;
        }
      });
    });
    if (!topFitnessCharacter) {
      throw new Error(constants.error.characterNotFound);
    }
    return topFitnessCharacter;
  },
  getAllCharactersForClassAndSpecFromAvailable(className: Class, spec: Specialization, availableCharacters: ClassStructure[]) {
    const characters = availableCharacters.find((x) => x.class == className)?.specsAndChars.find((x) => x.spec == spec)?.characters;
    if (!characters) {
      throw new Error(constants.error.characterNotFound);
    }
    return characters.filter((character) => {
      return _.maxBy(character.specDetails, function (o) {
        return o.fitness;
      });
    });
  },
  getAllCharactersForMainFromAvailable(mainName: string, availableCharacters: ClassStructure[]) {
    const availableCharactersCopy = [...availableCharacters];
    return availableCharactersCopy.map((a) => {
      return {
        ...a,
        specsAndChars: (a.specsAndChars = a.specsAndChars?.map((x) => {
          return {
            ...x,
            characters: x.characters.filter((y) => y.mainName === mainName),
          } as SpecAndCharacters;
        })),
      } as ClassStructure;
    });
  },
  addCharacterToRaidCompositionAs(
    character: Character,
    spec: Specialization,
    raidComposition: RaidComposition,
    slotNumber: number,
    isEmpty: boolean = false,
    realm?: RSRealm
  ) {
    return raidComposition.slots.push({
      character,
      slotNumber,
      spec: spec,
      isEmpty,
      realm,
    });
  },
  getBenchedPlayers(availableCharacters: ClassStructure[]) {
    let benchedPlayers: string[] = [];
    availableCharacters.map((x) => {
      x.specsAndChars.map((y) => {
        y.characters.map((z) => {
          if (!benchedPlayers.includes(z.mainName)) {
            benchedPlayers.push(z.mainName);
          }
        });
      });
    });
    return benchedPlayers;
  },
  markSlotAsEmpty(slotNumber: number, slotRole: ClassAndSpec, raidComposition: RaidComposition) {
    this.addCharacterToRaidCompositionAs(
      {
        className: slotRole.class,
        mainName: constants.sheets.generatedCompositions.emptySlotMainName,
        name: constants.sheets.generatedCompositions.emptySlotName,
        specDetails: [],
      },
      slotRole.spec,
      raidComposition,
      slotNumber,
      true
    );
  },
  isSlotFilled(slotNumber: number, raidComposition: RaidComposition) {
    return raidComposition.slots.some((x) => x.slotNumber == slotNumber);
  },
  anyEmptySlot(raidSlots: RaidSlot[]) {
    return raidSlots.some((x) => x.isEmpty);
  },
  checkIfRaidCompositionNeedsBuffer(raidSlots: RaidSlot[], buffers: ClassAndSpec[]) {
    return !this.raidSlotContainsAnyClassAndSpec(raidSlots, buffers);
  },
  raidSlotContainsAnyClassAndSpec(raidSlots: RaidSlot[], classAndSpec: ClassAndSpec[]) {
    if (raidSlots.length == 0) {
      return false;
    }
    var x = raidSlots.find((x) => classAndSpec.some((y) => y.class === x.character.className && y.spec === x.spec));
    return x;
  },
  getNeededBuffersFromSlot(raidSlots: RaidSlot[], classAndSpecs: ClassAndSpec[]) {
    let neededBuffers: ClassAndSpec[] = [];
    classAndSpecs.map((x) => {
      const mangle = classService.getMangleClasses(); // ToDo: Refactor this
      if (this.isClassAndSpecInBufferGroup(x, mangle) && this.checkIfRaidCompositionNeedsBuffer(raidSlots, mangle)) {
        neededBuffers.push(x);
      }
      let meleeHaste = classService.getMeleeHasteClasses();
      if (this.isClassAndSpecInBufferGroup(x, meleeHaste) && this.checkIfRaidCompositionNeedsBuffer(raidSlots, meleeHaste)) {
        neededBuffers.push(x);
      }
      let rangeHaste = classService.getRangeHasteClasses();
      if (this.isClassAndSpecInBufferGroup(x, rangeHaste) && this.checkIfRaidCompositionNeedsBuffer(raidSlots, rangeHaste)) {
        neededBuffers.push(x);
      }
    });
    return neededBuffers;
  },
  isClassAndSpecBuffer(classAndSpec: ClassAndSpec) {
    const mangle = classService.getMangleClasses(); // ToDo: Refactor this
    if (this.isClassAndSpecInBufferGroup(classAndSpec, mangle)) {
      return true;
    }
    let meleeHaste = classService.getMeleeHasteClasses();
    if (this.isClassAndSpecInBufferGroup(classAndSpec, meleeHaste)) {
      return true;
    }
    let rangeHaste = classService.getRangeHasteClasses();
    if (this.isClassAndSpecInBufferGroup(classAndSpec, rangeHaste)) {
      return true;
    }
    return false;
  },
  isClassAndSpecInBufferGroup(classAndSpec: ClassAndSpec, bufferGroup: ClassAndSpec[]) {
    return bufferGroup.some((x) => _.isEqual(x, classAndSpec));
  },
  getSlotByClassAndSpec(classAndSpec: ClassAndSpec, raidSlot: TemplateRaidSlot[], currentRaidComposition: RaidComposition) {
    const allSlotsWithClassAndSpec = raidSlot.filter(
      (x) =>
        x.possibleClassAndSpecs.some((y) => y.class == classAndSpec.class && y.spec == classAndSpec.spec) &&
        !currentRaidComposition.slots.some((y) => y.slotNumber == x.number)
    );
    return _.orderBy(allSlotsWithClassAndSpec, function (o) {
      return o.possibleClassAndSpecs.length;
    });
  },
};
