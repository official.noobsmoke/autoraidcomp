import { constants } from "../constants";
import { sheetMapper } from "../mapper/sheetMapper";
import { spreadsheet } from "../server/spreadsheet";
import { generateCompositions } from "../ui/generateCompositions";

let compositionTemplates = [] as CompositionTemplate[];

export const compositionTemplatesService = {
  loadSpreadsheetData() {
    ReadCompositionTemplates();
  },
  getCompositionTemplateNames() {
    return compositionTemplates.map((template) => template.name);
  },
  getCompositionTemplateByName(name: string) {
    const compositionTemplate = compositionTemplates.find((template) => template.name === name);
    if (!compositionTemplate) {
      throw new Error(constants.error.compositionTemplateNotFound.format(name));
    }
    return compositionTemplate;
  },
  getRequiredCharactersForSpec(className: Class, spec: Specialization, composition: CompositionTemplate) {
    let count = 0;
    composition.raidSlots.map((x) => {
      if (x.possibleClassAndSpecs.some((y) => y.class === className && y.spec === spec)) {
        count += 1 / x.possibleClassAndSpecs.length;
      }
    });
    return count;
  },
  getCurrentCompositionTemplateByName() {
    return compositionTemplatesService.getCompositionTemplateByName(generateCompositions.getSelectedCompositionTemplatePosition());
  },
};

const ReadCompositionTemplates = () => {
  const instanceFunctions = {
    [Instance.icc]: ReadICCSettings,
    [Instance.rs]: ReadRSSettings,
  };
  const data = spreadsheet.getSheetValues(constants.sheets.compositionTemplates.name);
  for (let i = 0; i < data.length; i++) {
    let compositionTemplate = sheetMapper.sheetRowToCompositionTemplate(data[i]);
    instanceFunctions[compositionTemplate.instance](i + 1, compositionTemplate, data);
    i += compositionTemplate.playersCount;
    compositionTemplates.push(compositionTemplate);
  }
};

const ReadICCSettings = (nextRowNumber: number, compositionTemplate: CompositionTemplate, data: any) => {
  let slotCount = 1;
  for (let i = nextRowNumber; i < nextRowNumber + compositionTemplate.playersCount; i++) {
    const raidSlot = sheetMapper.sheetICCRowToRaidSlot(data[i]);
    raidSlot.number = slotCount;
    compositionTemplate.raidSlots.push(raidSlot);
    slotCount++;
  }
};

const ReadRSSettings = (nextRowNumber: number, compositionTemplate: CompositionTemplate, data: any) => {
  let slotCount = 1;
  for (let i = nextRowNumber; i < nextRowNumber + compositionTemplate.playersCount; i++) {
    const raidSlot = sheetMapper.sheetRSRowToRaidSlot(data[i]);
    raidSlot.number = slotCount;
    compositionTemplate.raidSlots.push(raidSlot);
    slotCount++;
  }
};
