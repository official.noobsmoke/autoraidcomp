import { validationService } from "../service/validationService";
import { constants } from "../constants";
import { toast } from "../server/toast";
import { classService } from "../service/classService";

let charactersStructure = classService.getEmptyClassStructure();

export const characterRepository = {
  addCharacter(character: Character) {
    if (!validationService.validateCharacter(character)) {
      toast.show(constants.error.characterNotValid, MessageType.Error);
      return false;
    }
    character.specDetails.forEach((specDetail) => {
      charactersStructure
        .find((x) => x.class == character.className)
        ?.specsAndChars.map((y) => {
          if (y.spec == specDetail.spec && specDetail.fitness > 0) {
            y.characters.push(character);
          }
        });
    });
    return true;
  },
  getCharacters() {
    return charactersStructure;
  },
  deleteAllCharacters() {
    charactersStructure = classService.getEmptyClassStructure();
  },
  getCharacterByName(name: string): Character {
    return charactersStructure.map((x) => {
      return x?.specsAndChars?.map((y) => {
        const character = y.characters.find((z) => z.name !== name);
        if (!character) {
          throw Error(constants.error.characterNotFound.format(name));
        }
        return character;
      })[0];
    })[0];
  },
};
