export const constants = {
  sheets: {
    importedCharacters: {
      name: "1.Imported Characters",
    },
    availablePlayers: {
      name: "2.Available Players",
    },
    compositionTemplates: {
      name: "3.Composition Templates",
    },
    generateCompositions: {
      name: "4.Generate Compositions",
      selectedCompositionTemplatePosition: "B1",
      maxNumberOfCompositionsPosition: "B2",
      countCompositionClassAndSpecStatisticsKey: "countCompositionClassAndSpecStatistics",
    },
    generatedCompositions: {
      name: "Generated Compositions",
      emptySlotName: "<empty>",
      emptySlotMainName: "<empty>",
    },
    settings: {
      name: "Settings",
    },
    savedCharacters: {
      name: "Saved Characters",
    },
  },
  error: {
    sheetNotFound: "Sheet '{0}' not found",
    characterNotValid: "Character is not valid",
    settingNotFound: "Setting '{0}' not found",
    classNameNotValid: "Class name is not valid for {0}",
    specNotValid: "One of the character specs is not valid {0}",
    compositionTemplateNotFound: "Composition template '{0}' not found",
    characterNotFound: "Character '{0}' not found",
    classNotFound: "Class '{0}' not found",
  },
};
