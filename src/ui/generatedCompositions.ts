import { constants } from "../constants";
import { spreadsheet } from "../server/spreadsheet";

export const generatedCompositions = {
  displayCompositions(raidCompositions: RaidComposition[]) {
    const displayFunctions = {
      [Instance.icc]: displayICC,
      [Instance.rs]: displayRS,
    };
    const sheet = spreadsheet.getSheetByName(constants.sheets.generatedCompositions.name);
    sheet.getDataRange().clearContent();
    displayFunctions[raidCompositions[0].instance](sheet, raidCompositions);
  },
};

const displayICC = (sheet: any, raidCompositions: RaidComposition[]) => {
  let currentRow = 1;
  for (let i = 0; i < raidCompositions.length; i++) {
    sheet.getRange(currentRow, 1).setValue(raidCompositions[i].number);
    currentRow++;
    for (let j = 0; j < raidCompositions[i].slots.length; j++) {
      sheet.getRange(currentRow, 1).setValue(raidCompositions[i].slots[j].character.name);
      sheet.getRange(currentRow, 2).setValue(raidCompositions[i].slots[j].character.mainName);
      sheet.getRange(currentRow, 3).setValue(raidCompositions[i].slots[j].character.className);
      sheet.getRange(currentRow, 4).setValue(raidCompositions[i].slots[j].spec);
      currentRow++;
    }
    sheet.getRange(currentRow, 1).setValue(raidCompositions[i].benchedPlayers.join(","));
    currentRow++;
  }
};

const displayRS = (sheet: any, raidCompositions: RaidComposition[]) => {
  let currentRow = 1;
  for (let i = 0; i < raidCompositions.length; i++) {
    sheet.getRange(currentRow, 1).setValue(raidCompositions[i].number);
    currentRow++;
    for (let j = 0; j < raidCompositions[i].slots.length; j++) {
      const slot = raidCompositions[i].slots[j];
      sheet.getRange(currentRow, 1).setValue(slot.realm);
      sheet.getRange(currentRow, 2).setValue(slot.character.name);
      sheet.getRange(currentRow, 3).setValue(slot.character.mainName);
      sheet.getRange(currentRow, 4).setValue(slot.character.className);
      sheet.getRange(currentRow, 5).setValue(slot.spec);
      currentRow++;
    }
    sheet.getRange(currentRow, 1).setValue(raidCompositions[i].benchedPlayers.join(","));
    currentRow++;
  }
};
