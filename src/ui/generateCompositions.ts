import { constants } from "../constants";
import { compositionTemplatesService } from "../service/compositionTemplatesService";
import { spreadsheet } from "../server/spreadsheet";
import { compositionGeneratorService } from "../service/compositionGeneratorService";
import { characterRepository } from "../repository/characterRepository";

export const generateCompositions = {
  load() {
    this.fillCompositionTemplateDropdown();
  },
  fillCompositionTemplateDropdown() {
    const sheet = spreadsheet.getSheetByName(constants.sheets.generateCompositions.name);
    const names = compositionTemplatesService.getCompositionTemplateNames();
    var cell = sheet.getRange(constants.sheets.generateCompositions.selectedCompositionTemplatePosition);
    var rule = SpreadsheetApp.newDataValidation().requireValueInList(names, true).build();
    cell.setDataValidation(rule);
  },
  getSelectedCompositionTemplatePosition() {
    return spreadsheet.getSheetCellValue(
      constants.sheets.generateCompositions.name,
      constants.sheets.generateCompositions.selectedCompositionTemplatePosition
    );
  },
  getMaxNumberOfCompositionsPosition(): number {
    return spreadsheet.getSheetCellValue(
      constants.sheets.generateCompositions.name,
      constants.sheets.generateCompositions.maxNumberOfCompositionsPosition
    );
  },
  countCompositionClassAndSpecStatistics() {
    const compositionTemplate = compositionTemplatesService.getCurrentCompositionTemplateByName();
    let result: PreviewSetupResult = {
      possibleCompsEstimation: {} as PossibleCompsEstimation,
      charactersStatisticInfo: [],
    };
    const allCharacters = characterRepository.getCharacters();
    result.charactersStatisticInfo = compositionGeneratorService.getAvailableAndRequiredStatistics(compositionTemplate, allCharacters);
    result.possibleCompsEstimation = compositionGeneratorService.calculatePossibleCompositionsEstimation(result.charactersStatisticInfo);
    return result;
  },
};
