export const sheetMapper = {
  sheetRowToCharacter(row: any) {
    let specs: SpecDetail[] = [];
    for (let i = 3; i < row.length; i += 2) {
      if (row[i] === "" || row[i + 1] === "") {
        break;
      }
      specs.push({ spec: row[i], fitness: row[i + 1] });
    }
    return {
      name: row[0],
      mainName: row[1],
      className: row[2],
      specDetails: specs,
    } as Character;
  },
  sheetRowToSavedCharacter(row: any) {
    let instances: Instance[] = [];
    for (let i = 1; i < row.length; i++) {
      instances.push(row[i]);
    }
    return {
      characterName: row[0],
      instances,
    } as SavedCharacter;
  },
  sheetRowToSetting(row: any) {
    return {
      name: row[0],
      value: row[1],
    } as Setting;
  },
  sheetRowToCompositionTemplate(row: any) {
    return {
      name: row[0],
      instance: row[1],
      playersCount: row[2],
      raidSlots: [],
    } as CompositionTemplate;
  },
  sheetICCRowToRaidSlot(row: any) {
    let classAndSpec: ClassAndSpec[] = [];
    for (let i = 0; i < row.length; i += 2) {
      if (row[i] === "" || row[i + 1] === "") {
        break;
      }
      classAndSpec.push({
        class: row[i],
        spec: row[i + 1],
      });
    }
    return {
      number: 0,
      possibleClassAndSpecs: classAndSpec,
    } as TemplateRaidSlot;
  },
  sheetRSRowToRaidSlot(row: any) {
    let classAndSpec: ClassAndSpec[] = [];
    for (let i = 1; i < row.length; i += 2) {
      if (row[i] === "") {
        break;
      }
      classAndSpec.push({
        class: row[i],
        spec: row[i + 1],
      });
    }
    return {
      number: 0,
      realm: row[0],
      possibleClassAndSpecs: classAndSpec,
    } as TemplateRaidSlot;
  },
};
